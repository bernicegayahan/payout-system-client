import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { useState, useContext } from 'react';
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import { Router, useHistory } from 'react-router-dom'

export default function Login() {

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const { user, setUser } = useContext(UserContext);
    const history = useHistory();
     if (user.email !== null) {
        Router.push('/user/payout');
    } 

    const retrieveUserDetails = (accessToken) => {
       
        const options = {
            headers: { Authorization: `Bearer ${accessToken}` }
        }
       
        fetch('http://localhost:4000/api/users/details', options).then((response) => response.json()).then(data => {
            setUser({ email: data.email }) 
            Router.push('/user/payout')
        })
    }

    function login(e) {
        e.preventDefault()
        fetch("http://localhost:4000/api/users/login", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            }) 
        }).then(res => res.json()).then(data => {
            if (typeof data.accessToken !== "undefined") {
              
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
                Swal.fire({
                    icon: 'success',
                    title: 'Successfully Logged In'
                })

            } else {
                Swal.fire('Login Error', 'You may have registered using a different login procedure', 'error')
            }
        })
    }
   
    return (
        <Container >
            <Row className="justify-content-center">
            <h1 className="text-center"> Login Page </h1>
                <Col xs md="6">
            <Form onSubmit={(e) => login(e)}>

                {/*email add*/}
                <Form.Group>
                    <Form.Label>Email Address </Form.Label>
                    <Form.Control value={email} type="email" placeholder="Insert user email" onChange={(event) => setEmail(event.target.value)} required />
                </Form.Group>

                {/* password*/}
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control value={password} type="password" placeholder="Insert user password" onChange={(event) => setPassword(event.target.value)} required />
                </Form.Group>

                <Button 
                    onClick={() => {
                        history.push('/payout');
                    }}
                    type="submit" 
                    variant="primary" 
                    className="mt-3 mb-3 w-100">
                    Login
                    </Button>
                    <p className="text-center">No account yet? Sign up <a href="/register">here.</a></p>

                
                
            </Form>
            </Col>
            </Row>
        </Container>
    )
}

import Banner from '../components/Banner'
import React from 'react';


export default function Home() {
    const data = {
        title: "Employee Payout App",
        content: "Making your life easier",
        destination: "/login",
        label: "Try It Now!"
    } 
    return (
        <div>
            <Banner data={data} />
            
        </div>
    )
}
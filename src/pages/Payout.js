import { useState } from 'react'
import React from 'react'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import { DailyPayoutTable } from '../components/DailyPayoutTable';
import { WeeklyPayoutTable } from '../components/WeeklyPayoutTable';
import { MonthlyPayoutTable } from '../components/MonthlyPayoutTable';

function Payout() {
    const [startDate, setStartDate] = useState(new Date());
   /*  const [endDate, setEndDate] = useState(null);
    const onChange = (dates) => {
        const [start, end] = dates;
        setStartDate(start);
        setEndDate(end);
    };  */
    // To calculate the time difference of two dates
    //let diff_time = endDate.getTime() - startDate.getTime();

    // To calculate the no. of days between two dates
    //let diff_days = diff_time / (1000 * 3600 * 24);
    //let  diff_days2 = diff_days % 30
    
    /* 
      Note: Date Picker
        0 - Sunday
        1 - Monday
        2 - Tuesday
        3 - Wednesday
        4 - Thursday
        5 - Friday
        6 - Saturday
    */
    //console.log(onChange.getDay());
    //console.log(onChange.getDate());


    console.log(startDate.getDay());
    console.log(startDate.getDate());

    const EmployeesPayroll = () => {
        if (startDate.getDay() === 0) {
            if (startDate.getDate() === 30) {
                return (
                    <>
                        <DailyPayoutTable />
                        <MonthlyPayoutTable />
                    </>
                );
            } else {
                return <DailyPayoutTable />;
            }
        } else if (startDate.getDay() === 1) {
            if (startDate.getDate() === 30) {
                return (
                    <>
                        <DailyPayoutTable />
                        <MonthlyPayoutTable />
                    </>
                );
            } else {
                return <DailyPayoutTable />;
            }
        } else if (startDate.getDay() === 2) {
            if (startDate.getDate() === 30) {
                return (
                    <>
                        <DailyPayoutTable />
                        <MonthlyPayoutTable />
                    </>
                );
            } else {
                return <DailyPayoutTable />;
            }
        } else if (startDate.getDay() === 3) {
            if (startDate.getDate() === 30) {
                return (
                    <>
                        <DailyPayoutTable />
                        <MonthlyPayoutTable />
                    </>
                );
            } else {
                return <DailyPayoutTable />;
            }
        } else if (startDate.getDay() === 4) {
            if (startDate.getDate() === 30) {
                return (
                    <>
                        <DailyPayoutTable />
                        <MonthlyPayoutTable />
                    </>
                );
            } else {
                return <DailyPayoutTable />;
            }
        } else if (startDate.getDay() === 5) {
            if (startDate.getDate() === 30) {
                return (
                    <>
                        <DailyPayoutTable />
                        <WeeklyPayoutTable />
                        <MonthlyPayoutTable />
                    </>
                );
            } else {
                return (
                    <>
                        <DailyPayoutTable />
                        <WeeklyPayoutTable />
                    </>
                )
            }
        } else if (startDate.getDay() === 6) {
            if (startDate.getDate() === 30) {
                return (
                    <>
                        <DailyPayoutTable />
                        <MonthlyPayoutTable />
                    </>
                );
            } else {
                return (
                    <>
                        <DailyPayoutTable />
                    </>
                )
            }
        } else {
            return (
                <>
                    <h1>Something is wrong, please try again.</h1>
                </>
            )
        }
    }



    return (
        <>
            <h3 className="mt-3">Select a Date to Start</h3>
            <DatePicker className="datePicker"
                placeholderText="Click to select a date"
                selected={startDate}
                isClearable={true}
                shouldCloseOnSelect={false}
                onChange={(date) => setStartDate(date)}
            />

            <EmployeesPayroll />
        </>
    );
};

export default Payout;


import { Form, Button, Row, Container, Col } from 'react-bootstrap';
import { useState, useEffect } from 'react'
import Swal from 'sweetalert2';



export default function Register() {
    
    const [email, setEmail] = useState("") //assign an initial value.
    const [password1, setPassword1] = useState("")
    const [password2, setPassword2] = useState("")
    const [btnActive, setBtnActive] = useState(false)

    
    function registerUser(e) {
        e.preventDefault() 


        fetch('http://localhost:4000/api/users/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password1
            })
        }).then(res => res.json()).then(convertedData => {
            console.log(convertedData)
            if (convertedData === true) {
                Swal.fire({
                    icon: "success",
                    title: "Successfully Registered!",
                    text: "Thank you for registering."
                })

                setEmail("")
                setPassword1("")
                setPassword2("")
                
            } else {
                Swal.fire({
                    icon: "error",
                    title: "Registration Failed!!",
                    text: "Something went wrong, please try again later."
                })
            }
        })
    }
    
    useEffect(() => {
        
        if ((email !== "" && password1 !== "" && password2 !== "") && (password2 === password1)) {
            setBtnActive(true)
        } else {
            setBtnActive(false)
        }
    }, [email, password1, password2])

    return (
        <Container>

            <Row className="justify-content-center">
                <h2 className="mt-5 text-center"> Register New User </h2>
                <Col xs md="6">
                
                    <Form onSubmit={(e) => registerUser(e)}>
                        {/*user's email*/}
                        <Form.Group>
                            <Form.Label>User Email Address</Form.Label>
                            <Form.Control onChange={e => setEmail(e.target.value)} value={email} type="email" placeholder="Insert Email Address Here" required />
                        
                        </Form.Group>
                        {/*password*/}
                        <Form.Group>
                            <Form.Label>Password</Form.Label>
                            <Form.Control onChange={e => setPassword1(e.target.value)} value={password1} type="password" placeholder="Insert Password Here" required />
                        </Form.Group>
                        {/*confirm password*/}
                        <Form.Group>
                            <Form.Label>Confirm Password </Form.Label>
                            <Form.Control onChange={e => setPassword2(e.target.value)} value={password2} type="password" placeholder="Confirm Password Here" required />
                        </Form.Group>

                        
                        {btnActive ?
                            <Button type="submit" className="mt-3" variant="primary">Register!</Button>
                            :
                            <Button type="submit" className="mt-3" variant="primary" disabled>Register!</Button>
                        }


                    </Form>
                </Col>
            </Row>
        </Container>
    )
}

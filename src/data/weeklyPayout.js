const weeklyPayout = [
    {
        employeeNumber: "H0015",
        firstName: "Jessie",
        lastName: "Payumo",
        position: "Director of Photography",
        team: "Camera",
        salary: 8750,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0016",
        firstName: "Stephan",
        lastName: "Acuesta",
        position: "1st Assistant Camera",
        team: "Camera",
        salary: 4000,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0017",
        firstName: "Damion",
        lastName: "Araneta",
        position: "2nd Assistant Camera",
        team: "Camera",
        salary: 3750,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0018",
        firstName: "Shawn",
        lastName: "Despujol",
        position: "Stedicam Operator",
        team: "Camera",
        salary: 3250,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0019",
        firstName: "Humberto",
        lastName: "Panopio",
        position: "Key Grip",
        team: "Grip",
        salary: 3500,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0020",
        firstName: "Cesario",
        lastName: "Lorete",
        position: "Dolly Grip",
        team: "Grip",
        salary: 3250,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0021",
        firstName: "Semaj",
        lastName: "Bristol",
        position: "Rigging Grips",
        team: "Grip",
        salary: 3000,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0022",
        firstName: "Keyon",
        lastName: "Mangahas",
        position: "Gaffer",
        team: "Electric",
        salary: 5000,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0023",
        firstName: "Lance",
        lastName: "Sulit",
        position: "Best Boy Electric",
        team: "Electric",
        salary: 4250,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0024",
        firstName: "Ibrahim",
        lastName: "Rendón",
        position: "Electrician",
        team: "Electric",
        salary: 4000,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0025",
        firstName: "Deshaun",
        lastName: "Osorio",
        position: "Generator Operator",
        team: "Electric",
        salary: 3500,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0026",
        firstName: "Wade",
        lastName: "Atayde",
        position: "Production Designer",
        team: "Art",
        salary: 7500,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0027",
        firstName: "Fidele",
        lastName: "Isidro",
        position: "Art Director",
        team: "Art",
        salary: 10500,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0028",
        firstName: "Devin",
        lastName: "Gahol",
        position: "Art Department Coordinator",
        team: "Art",
        salary: 9500,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0029",
        firstName: "Erik",
        lastName: "Magallanes",
        position: "Construction Coordinator",
        team: "Art",
        salary: 5000,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0030",
        firstName: "Freddie",
        lastName: "Montero",
        position: "Carpenter",
        team: "Art",
        salary: 3500,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0031",
        firstName: "Sonny",
        lastName: "Balagtas",
        position: "Scenic Artist",
        team: "Art",
        salary: 4000,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0032",
        firstName: "Antoine",
        lastName: "Batac",
        position: "Set Decorator",
        team: "Art",
        salary: 3000,
        paymentCycle: "weekly",

    },
    {
        employeeNumber: "H0033",
        firstName: "Charlotte",
        lastName: "Monteverde",
        position: "Set Dresser",
        team: "Art",
        salary: 3000,
        paymentCycle: "weekly",

    }
]
export default weeklyPayout;
const monthlyPayout = [
    {
        employeeNumber: "H001",
        firstName: "Vicente",
        lastName: "Mangahas",
        position: "Director",
        team: "Production",
        salary: 73000,
        paymentCycle: "monthly",

    },
    {
        employeeNumber: "H002",
        firstName: "Michael",
        lastName: "Silvestre",
        position: "Producer",
        team: "Production",
        salary: 48000,
        paymentCycle: "monthly",

    },
    {
        employeeNumber: "H003",
        firstName: "Severo",
        lastName: "Matías",
        position: "Line Producer",
        team: "Production",
        salary: 31000,
        paymentCycle: "monthly",

    },
    {
        employeeNumber: "H004",
        firstName: "Aricela",
        lastName: "Montelibano",
        position: "Assistant Director",
        team: "Production",
        salary: 29000,
        paymentCycle: "monthly",

    },
    {
        employeeNumber: "H005",
        firstName: "Alexzander",
        lastName: "Halili",
        position: "Unit Production Manager",
        team: "Production",
        salary: 27000,
        paymentCycle: "monthly",

    },
    {
        employeeNumber: "H006",
        firstName: "Terrance",
        lastName: "Galéndez",
        position: "Production Coordinator",
        team: "Production",
        salary: 16000,
        paymentCycle: "monthly",

    },
    {
        employeeNumber: "H007",
        firstName: "Harry",
        lastName: "Capistrano",
        position: "Production Secretary",
        team: "Production",
        salary: 15000,
        paymentCycle: "monthly",

    },
    {
        employeeNumber: "H008",
        firstName: "Holly",
        lastName: "Pelayo",
        position: "Office PA",
        team: "Production",
        salary: 14000,
        paymentCycle: "monthly",

    },
    {
        employeeNumber: "H009",
        firstName: "Louredes",
        lastName: "Acuña",
        position: "Location Manager",
        team: "Locations",
        salary: 25000,
        paymentCycle: "monthly",

    },
    {
        employeeNumber: "H0010",
        firstName: "Josefina",
        lastName: "Carrasco",
        position: "Location Assistants",
        team: "Locations",
        salary: 19000,
        paymentCycle: "monthly",

    },
    {
        employeeNumber: "H0011",
        firstName: "Annabelle",
        lastName: "Austria",
        position: "Unit PA",
        team: "Locations",
        salary: 15000,
        paymentCycle: "monthly",

    },
    {
        employeeNumber: "H0012",
        firstName: "Jerardo",
        lastName: "Alejo",
        position: "Sound Mixer",
        team: "Sound",
        salary: 17000,
        paymentCycle: "monthly",

    },
    {
        employeeNumber: "H0013",
        firstName: "Marquis",
        lastName: "Celiz",
        position: "Boom Operator",
        team: "Sound",
        salary: 14000,
        paymentCycle: "monthly",

    },
    {
        employeeNumber: "H0014",
        firstName: "Noe",
        lastName: "Dugong",
        position: "Sound Utility",
        team: "Sound",
        salary: 12000,
        paymentCycle: "monthly",

    }
]
export default monthlyPayout;
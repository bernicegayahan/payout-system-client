const dailyPayout = [
    {
        employeeNumber: "H0034",
        firstName: "Ty",
        lastName: "Fandiño",
        position: "Prop Master",
        team: "Props",
        salary: 800,
        paymentCycle: "daily",

    },
    {
        employeeNumber: "H0035",
        firstName: "Salvatore",
        lastName: "Atayde",
        position: "Assistant Prop Master",
        team: "Props",
        salary: 600,
        paymentCycle: "daily",

    },
    {
        employeeNumber: "H0036",
        firstName: "Sam",
        lastName: "Areopagita",
        position: "Prop Maker",
        team: "Props",
        salary: 550,
        paymentCycle: "daily",

    },
    {
        employeeNumber: "H0037",
        firstName: "Ivelisse",
        lastName: "Ignacio",
        position: "Prop Assistant",
        team: "Props",
        salary: 550,
        paymentCycle: "daily",

    },
    {
        employeeNumber: "H0038",
        firstName: "Alisha",
        lastName: "Buendía",
        position: "Costume Designer",
        team: "Costume and Wardrobe",
        salary: 900,
        paymentCycle: "daily",

    },
    {
        employeeNumber: "H0039",
        firstName: "Alanis",
        lastName: "Vitug",
        position: "Assistant Costume Designer",
        team: "Costume and Wardrobe",
        salary: 700,
        paymentCycle: "daily",

    },
    {
        employeeNumber: "H0040",
        firstName: "Kaitlin",
        lastName: "Jalandoni",
        position: "Shopper",
        team: "Costume and Wardrobe",
        salary: 800,
        paymentCycle: "daily",

    },
    {
        employeeNumber: "H0041",
        firstName: "Talia",
        lastName: "Rillo",
        position: "Wardrobe Supervisor",
        team: "Costume and Wardrobe",
        salary: 850,
        paymentCycle: "daily",

    },
    {
        employeeNumber: "H0042",
        firstName: "Halie",
        lastName: "Alegre",
        position: "Set Costumes",
        team: "Costume and Wardrobe",
        salary: 650,
        paymentCycle: "daily",

    },
    {
        employeeNumber: "H0043",
        firstName: "Carleigha",
        lastName: "Maranan",
        position: "Hair Department Head",
        team: "Hair and Makeup",
        salary: 1500,
        paymentCycle: "daily"

    },
    {
        employeeNumber: "H0044",
        firstName: "Corazana",
        lastName: "Soriano",
        position: "Key Hair Department",
        team: "Hair and Makeup",
        salary: 1000,
        paymentCycle: "daily"

    },
    {
        employeeNumber: "H0045",
        firstName: "Tiara",
        lastName: "Digamon",
        position: "Makeup Department Head",
        team: "Hair and Makeup",
        salary: 1500,
        paymentCycle: "daily",

    },
    {
        employeeNumber: "H0046",
        firstName: "Anne",
        lastName: "Guerra",
        position: "Key Makeup Department",
        team: "Hair and Makeup",
        salary: 1000,
        paymentCycle: "daily",

    },
    {
        employeeNumber: "H0047",
        firstName: "Elise",
        lastName: "Mariano",
        position: "Special Effects Makeup",
        team: "Hair and Makeup",
        salary: 2000,
        paymentCycle: "daily",

    }
]
export default dailyPayout;

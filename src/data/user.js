export default [
    {
        username: "admin",
        password: "admin123",
        isAdmin: true
    },
    {
        username: "john_doe",
        password: "user123",
        isAdmin: false
    },
    {
        username: "jane_doe",
        password: "user1234",
        isAdmin: false
    }
]
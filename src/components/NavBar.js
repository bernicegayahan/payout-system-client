import { Navbar, Container, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';

export default function NavBar() {
    
    return (
        <Navbar className="justify-content-between" expand="lg" variant="dark" bg="dark">
            <Container>
                <Navbar.Brand as={Link} to="/">Payout System</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                         <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                         <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                         <Nav.Link as={NavLink} to="/payout">Payout Scheme</Nav.Link>
                         <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link> 
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}
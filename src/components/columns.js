import React from 'react'

export const COLUMNS = [
    {
        Header: 'Employee Number',
        accessor: 'employeeNumber',
        //Footer: 'Employee Number'
    },
    {
        Header: 'First Name',
        accessor: 'firstName',
        //Footer: 'First Name'
    },
    {
        Header: 'Last Name',
        accessor: 'lastName',
        //Footer: 'Last Name'
    },
    {
        Header: 'Position',
        accessor: 'position',
        //Footer: 'Position'
    },
    {
        Header: 'Team',
        accessor: 'team',
        //Footer: 'Team'
    },
    {
        Header: 'Payment Cycle',
        accessor: 'paymentCycle',
        //Footer: 'Payment Cycle'
    },
    {
        Header: 'Salary',
        accessor: 'salary',
        //Footer: 'Salary'
    },
    {
        Header: 'Total',
        accessor: 'total',
        Footer: info => {
            const total = React.useMemo(
                () =>
                    info.rows.reduce((sum, row) => row.values.salary + sum, 0),
                [info.rows]
            )

            return <>Total: {total}</>
        },
    }
     
]

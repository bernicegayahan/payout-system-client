import { Jumbotron, Container, Button } from 'react-bootstrap'
import { Row, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom';

 const Banner = ({ data }) => {
    
    const { title, content, destination, label } = data
    return (
        <Container>
            <Row>
                <Col>
                    <Jumbotron className="jumbo">
                        <h1> {title} </h1>
                        <p> {content} </p>
                        <Button variant="primary" className="mb-5" as={Link} to={destination}> {label} </Button>
                    </Jumbotron>
                </Col>
            </Row>
        </Container>
    )
}

export default Banner;
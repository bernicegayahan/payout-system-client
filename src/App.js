import NavBar from './components/NavBar'
import Home from './pages/Home'
import { Container } from 'react-bootstrap';
import Register from './pages/Register'
import Login from './pages/Login'
import Payout from './pages/Payout'
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import { UserProvider } from './UserContext'
import Logout from './pages/Logout';
import { useState } from 'react';
import './App.css';
import 'react-datepicker/dist/react-datepicker.css'



export default function App() {

  const [user, setUser] = useState({
    email: localStorage.getItem('email')
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <>
      <UserProvider value={{ unsetUser, user, setUser }}>
        <Router>
          <NavBar />
          <Container>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/payout" component={Payout} />
              <Route exact path="/logout" component={Logout} />
            </Switch>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}
  



